package app.common;

import java.util.Collections;
import java.util.List;

import org.durcframework.core.MessageResult;

public class ApiMessageResult implements MessageResult {

	private static final String CODE_SUCCESS = "0";
	
	private String code = CODE_SUCCESS;
	private String message = "";
	private List<String> messages = Collections.emptyList();
	
	public static ApiMessageResult success(String message) {
		ApiMessageResult result = success();
		result.setMessage(message);
		return result;
	}

	public static ApiMessageResult success() {
		ApiMessageResult result = new ApiMessageResult();
		result.setCode(CODE_SUCCESS);
		return result;
	}

	public static ApiMessageResult error(String code,String errorMsg) {
		ApiMessageResult result = new ApiMessageResult();
		result.setCode(code);
		result.setSuccess(false);
		result.setMessage(errorMsg);
		return result;
	}

	public static ApiMessageResult error(String code,String errorMsg, List<String> errors) {
		ApiMessageResult result = error(code,errorMsg);
		result.setMessages(errors);
		return result;
	}

	@Override
	public void setSuccess(boolean success) {
		if(success) {
			this.code = CODE_SUCCESS;
		}
	}

	@Override
	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public void setMessages(List<String> messages) {
		this.messages = messages;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public List<String> getMessages() {
		return messages;
	}

}

package app.common;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.durcframework.core.MessageResult;
import org.durcframework.core.controller.BaseController;
import org.durcframework.open.OpenException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

public class ApiController extends BaseController {

	@Override
	protected MessageResult getMessageResult() {
		return new ApiMessageResult();
	}

	@ExceptionHandler
	protected 
	@ResponseBody 
	MessageResult exceptionHandler(HttpServletRequest request,
			HttpServletResponse response,
			OpenException e) {
		return error(e.getCode(),e.getMessage());
    }
	
	/**
	 * 返回成功的视图
	 * @return 默认返回DefaultMessageResult对象,可以重写getMessageResult()方法返回自定义的MessageResult
	 */
	public MessageResult success() {
		return ApiMessageResult.success();
	}
	
	/**
	 * 返回成功
	 * @param message
	 * @return 默认返回DefaultMessageResult对象,可以重写getMessageResult()方法返回自定义的MessageResult
	 */
	public MessageResult success(String message) {
		return ApiMessageResult.success(message);
	}
	
	/**
	 * 返回错误的视图
	 * @param errorMsg 错误信息
	 * @return 默认返回DefaultMessageResult对象,可以重写getMessageResult()方法返回自定义的MessageResult
	 */
	public MessageResult error(String code,String errorMsg) {
		return ApiMessageResult.error(code, errorMsg);
	}
	
	/**
	 * 返回错误信息
	 * @param errorMsg 错误信息
	 * @param errorMsgs 更多错误信息
	 * @return 默认返回DefaultMessageResult对象,可以重写getMessageResult()方法返回自定义的MessageResult
	 */
	public MessageResult error(String code,String errorMsg,List<String> errorMsgs) {
		return ApiMessageResult.error(code, errorMsg, errorMsgs);
	}

}

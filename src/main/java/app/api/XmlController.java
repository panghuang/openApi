package app.api;

import java.util.Date;

import org.durcframework.open.annotation.ApiMethod;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import app.common.ApiController;
import app.entity.Student;

@Controller
public class XmlController extends ApiController{

	// 使用IE浏览器访问
	// 请求头决定了返回格式
	@RequestMapping("/xmlTest.do")
	@ApiMethod(needSign=false)
	public 
	@ResponseBody Student xmlTest(){
		
		Student stu = new Student();
		stu.setAddress("aaaa");
		stu.setBirthday(new Date());
		stu.setId(1);
		
		return stu;
	}
}

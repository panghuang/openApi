package app.api;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import app.ApiTestBase;

public class StudentAPICrudControllerTest extends ApiTestBase {
	
	// http://localhost/openApi/listApiStudent.do?appId=test&timestamp=330523156522&schName=Jim&sign=45FF792F8281667B3F8EF1CAC75FFAA0DDBD31E2
	@Test  
	public void testTest1() {
		// Map作为参数
		Map<String,String> params = new HashMap<String, String>();
		params.put("schName", "Jim");
		
		this.getJSON("/listApiStudent.do", params);
		//this.getXML("/test1.do", params);
	}  
	
}

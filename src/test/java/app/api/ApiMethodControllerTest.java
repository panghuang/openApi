package app.api;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import app.ApiTestBase;

public class ApiMethodControllerTest extends ApiTestBase {
	
	@Test  
	public void testTest1() {
		// Map作为参数
		Map<String,String> params = new HashMap<String, String>();
		params.put("schName", "Jim");
		
		this.getJSON("/test1.do", params);
		//this.getXML("/test1.do", params);
	}  
	
}
